# Aspose for Spring Java

Aspose for Spring.Java provides usage samples / sourcecodes for the demonstration of Aspose API for Java by extending famous Spring Java PetClinic Sample Web Application.

This extension of "Spring Java PetClinic Sample Web Application" also shows the Integration of Aspose APIs with Spring MVC, JSP and Maven Project.

This extension helps you to explore Aspose Java components within Spring Framework / Spring MVC /Maven environments

# Aspose Extension for Spring PetClinic Sample Web Application

## What does it look like?
-spring-petclinic has been deployed here on cloudfoundry: http://demo-spring-petclinic.cfapps.io/

## For Aspose API for Java see url : http://www.aspose.com/java/total-component.aspx

## Latest Release v1.1
**What's New**

1.    Local Aspose Maven Dependencies have been replaced with Aspose Centralized APIs Maven Dependencies supported by Aspose Cloud Maven Repository for Aspose.Words, Aspose.Pdf, Aspose.Cells, Aspose.Email and Aspose.Barcode Java APIs. See the pom.xml file.
2.    Size of the project has been reduced from 48 MB to 2 MB because of removal of local dependencies from project.
3.    UI enhancements
4.    Use case for showcasing Aspose.Words Java API feature of converting HTML to MS-WORD added by providing new option image of HTML to MS-WORD export on bottom of all webpages that converts the current web page (html) with images into MS-WORD file and save to disk.
5.    Bug fixes and to avoid PermGen errors, allocation defined for IntelliJ IDEA in .idea/workspace.xml configuration file.

**Release - v1.0** 

1.    On Veterinarians Page, Availability Days and Email Addresses are added, by Clicking on Email Address of any one will open Email Form where you can write email to the Veterinarian and send it through your Outlook Client by using Aspose APIs (Aspose.Email)
2.    On Veterinarians Page, You can export Veterinarians List to PDF, MS-Word and MS-Excel Formats using Aspose APIs (Aspose.PDF, Aspose.Words, Aspose.Cells)
3.    On Owners List Page, You can export Veterinarians List to PDF, MS-Word and MS-Excel Formats using Aspose APIs (Aspose.PDF, Aspose.Words, Aspose.Cells)
4.    On Add Visit Page, You can now also Feed Bill Amount in $ which will later display along with generated Barcode image on the Add Visit Page and Owners Information Page by using Aspose APIs (Aspose.Barcode)
5.    Export to PDF, MS-Word and MS-Excel documents (On Veterinarians Page & Owners List Page) also shows the sample usages of Aspose APIs for inserting Images, Formattings and Tables to the generated documents using Aspose.Words, Aspose.PDF and Aspose.Cells
6.    For creating / displaying Barcode Image using Aspose APIs (Aspose.Barcode) on JSP or any MVC Framework using JSP as Views, Tag Library <aspose:getBarcodeUrl> is created through which a Barcode Image can be very easily generated and embeded on the page providing CodeText / Amount and Symbology (Type of Barcode i.e Code128, QR) through tag attributes.
7.    Source code of usage of above features of Aspose API (Aspose.PDF,Aspose.Words, Aspose.Cells, Aspose.Email, Aspose.Barcode) are included in this project repository (Integrated with ready to run PetClinic Spring Sample Web Application).
8.    Source Codes of Tag Library for Aspose.Barcode usage for JSPs also included with the project sources code and demonstrated on Add/Edit Visit Page and Owners Information Page
9.    This Spring PetClinic Web Application extension for Aspose API also shows the Aspose APIs integration with Maven

# How to configure this extension of Spring Java PetClinic Sample Web Application for Aspose API

Below simple steps will smoothly lead to successful configuration of Spring Java PetClinic Sample Web Application for Aspose API source code in IntelliJ IDE / Eclipse IDE

## Running petclinic locally
```
	git clone https://github.com/asposemarketplace/Aspose_for_Spring.Java.git
	mvn tomcat7:run
```

You can then access petclinic here: http://localhost:9966/petclinic/


**Note**: In case of any PermGen related JVM error, adjust PermGen memory space by setting the appropriate values to MAVEN_OPTS environment variable.

For Example:

    export MAVEN_OPTS="-Xmx512m -XX:MaxPermSize=128m"

(or on Windows:)

    set MAVEN_OPTS=-Xmx512m -XX:MaxPermSize=128m


## Prerequisites

### Working with Petclinic in JetBrains

The following items should be installed in your system:

1.   Maven 3 (http://www.sonatype.com/books/mvnref-book/reference/installation.html)

2.   git command line tool (https://help.github.com/articles/set-up-git)

3.   IntelliJ IDEA (Ultimate/CE)

### Working with Petclinic in Eclipse/STS

The following items should be installed in your system:

1.   Maven 3 (http://www.sonatype.com/books/mvnref-book/reference/installation.html)

2.   git command line tool (https://help.github.com/articles/set-up-git)

3.   Eclipse with the m2e plugin (m2e is installed by default when using the STS (http://www.springsource.org/sts) distribution of Eclipse)

Note: when m2e is available, there is an m2 icon in Help -> About dialog.
If m2e is not there, just follow the install process here: http://eclipse.org/m2e/download/


### Steps:

1)   In the command line
```
git clone https://github.com/asposemarketplace/Aspose_for_Spring.Java.git
```
2)   Open Project

#### Inside Eclipse

File -> Import -> Maven -> Existing Maven project

#### Inside IntelliJ

File--> Open -> select spring-petclinic Project

## Recommended Links

[Youtube Video Demo of the project](https://www.youtube.com/watch?v=GLujBd6gq_Y&feature=youtu.be)

[Aspose Java Components](http://www.aspose.com/java/total-component.aspx)
    

## Understanding the Spring Petclinic application with a few diagrams
See the presentation here: https://speakerdeck.com/michaelisvy/spring-petclinic-sample-application